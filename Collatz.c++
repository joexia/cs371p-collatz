// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

//the cache
int cache [1000000] = { };

long collatz_help (long i) {
    //initialize return
    long n = 1;

    //keep going until hit 1
    while (i > 1) {
        //if less than 1000000 check in cache
        if (i < 1000001) {
            //if found, return
            if (cache[i] != 0) {
                return n + cache[i] - 1;
            }
        }

        //go through the algorithm
        if (i % 2 != 0) {
            i = i + (i >> 1) + 1;
            ++n;
        } else {
            i = i/2;
        }
        //increment cycle length
        ++n;
    }
    //return cycle length
    return n;
}
// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    //switch them so smaller number is first
    if( i > j) {
        long jcopy = j;
        j = i;
        i = jcopy;
    }
    //holds greatest collatz cycle length
    long max = 0;
    //holds current collatz cycle test length
    long test = 0;

    //optimization where we check from max/2 +1
    long checkLow = j/2 + 1;
    if (i < checkLow) {
        i = checkLow;
    }

    //cycle through, check each time
    for(long n = i; n <= j; ++n) {
        if (cache[n] == 0) {
            //add to cache if not there
            cache[n] = collatz_help(n);
        }
        //get from cache
        test = cache[n];

        //if higher cycle length, store that
        if (max < test) {
            max = test;
        }
    }

    //return highest found cycle length
    return max;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
